<?php
include 'db.php';

//class response for get data from db back

$get_sql = "SELECT name, condition, ST_AsGeoJSON(geom) FROM public.entries";
//save to array
$resultArray = pg_fetch_all(pg_query($dbcon,$get_sql));
//pass geojson as a array to java script
echo json_encode($resultArray);

?>